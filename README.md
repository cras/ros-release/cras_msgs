## cras_msgs (noetic) - 1.1.1-1

The packages in the `cras_msgs` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_msgs` on `Fri, 25 Aug 2023 15:39:25 -0000`

The `cras_msgs` package was released.

Version of package(s) in repository `cras_msgs`:

- upstream repository: https://github.com/ctu-vras/cras_msgs
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/cras_msgs
- rosdistro version: `1.1.0-1`
- old version: `1.1.0-1`
- new version: `1.1.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_msgs (noetic) - 1.1.0-1

The packages in the `cras_msgs` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_msgs` on `Wed, 03 May 2023 23:31:38 -0000`

The `cras_msgs` package was released.

Version of package(s) in repository `cras_msgs`:

- upstream repository: https://github.com/ctu-vras/cras_msgs
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/cras_msgs
- rosdistro version: `1.0.1-1`
- old version: `1.0.1-1`
- new version: `1.1.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_msgs (melodic) - 1.1.0-1

The packages in the `cras_msgs` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_msgs` on `Wed, 03 May 2023 23:25:02 -0000`

The `cras_msgs` package was released.

Version of package(s) in repository `cras_msgs`:

- upstream repository: https://github.com/ctu-vras/cras_msgs
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/cras_msgs
- rosdistro version: `1.0.1-1`
- old version: `1.0.1-2`
- new version: `1.1.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_msgs (melodic) - 1.0.1-2

The packages in the `cras_msgs` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_msgs` on `Wed, 03 May 2023 23:17:41 -0000`

The `cras_msgs` package was released.

Version of package(s) in repository `cras_msgs`:

- upstream repository: https://github.com/ctu-vras/cras_msgs
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/cras_msgs
- rosdistro version: `1.0.1-1`
- old version: `1.0.1-1`
- new version: `1.0.1-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_msgs (melodic) - 1.0.1-1

The packages in the `cras_msgs` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic cras_msgs --edit` on `Thu, 30 Mar 2023 16:35:47 -0000`

The `cras_msgs` package was released.

Version of package(s) in repository `cras_msgs`:

- upstream repository: https://github.com/ctu-vras/cras_msgs
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_msgs (noetic) - 1.0.1-1

The packages in the `cras_msgs` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic cras_msgs --edit` on `Thu, 30 Mar 2023 16:30:41 -0000`

The `cras_msgs` package was released.

Version of package(s) in repository `cras_msgs`:

- upstream repository: https://github.com/ctu-vras/cras_msgs
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


